package inf101v22.mockexam.set;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Set<T> implements ISet<T>{

    private List<T> set;

    public Set(){
        set = new ArrayList<>();
    }

	@Override
	public Iterator<T> iterator() {
        return set.listIterator();
	}

	@Override
	public int size() {
        return set.size();
	}

	@Override
	public void add(T element) {
		if (!set.contains(element)){
            set.add(element);
        }
	}

	@Override
	public void addAll(Iterable<T> other) {
		if (!(other == null)) {
			for (T item : other){
				set.add(item);
			}
		}
	}

	@Override
	public void remove(T element) {
        if (set.contains(element)){
        int index = set.indexOf(element);
        set.remove(index);
        }
	}

	@Override
	public boolean contains(T element) {
		return set.contains(element);
	}

	@Override
	public ISet<T> union(ISet<T> other) {
		Set<T> union = new Set<>();

		union.addAll(other);
		union.addAll(set);

		return union;
	}

	@Override
	public ISet<T> intersection(ISet<T> other) {
		Set<T> intersection = new Set<>();

		for (T item : other){
			if (set.contains(item)){
				intersection.add(item);
			}
		}

		return intersection;
	}

	@Override
	public ISet<T> complement(ISet<T> other) {
		Set<T> complement = new Set<>();

		for (T item : set){
			if (!other.contains(item)){
				complement.add(item);
			}
		}

		return complement;
	}

	@Override
	public ISet<T> copy() {
		Set<T> copySet = new Set<>();

		for (T item : set){
			copySet.add(item);
		}

		return copySet;
	}

    
}
