package inf101v22.mockexam.traffic.view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

import inf101v22.mockexam.traffic.model.TrafficLightViewable;

public class TrafficLightView extends JComponent{

    public TrafficLightView(TrafficLightViewable viewable){
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        LampView greenLampView = new LampView(viewable.greenIsOn(), Color.GREEN);
        LampView yellowLampView = new LampView(viewable.yellowIsOn(), Color.YELLOW);
        LampView redLampView = new LampView(viewable.redIsOn(), Color.RED);

        this.add(greenLampView);
        this.add(yellowLampView);
        this.add(redLampView);

    }

    
}
