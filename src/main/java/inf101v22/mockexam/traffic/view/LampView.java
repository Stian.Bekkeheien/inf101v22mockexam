package inf101v22.mockexam.traffic.view;

import inf101v22.mockexam.observable.Observable;
import inf101v22.mockexam.observable.Observer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;


public class LampView extends JComponent{
    private Observable<Boolean> light;
    private Color color;
    public LampView(Observable<Boolean> light, Color color){
        this.light = light;
        this.color = color;
        
        Observer observer = this::repaint;
        light.addObserver(observer);
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);

        if (light.getValue() == true){
            g.setColor(color);
        }

        else {
            g.setColor(Color.BLACK);
        }

        g.fillOval(0, 0, getWidth(), getHeight());
    }

    @Override
    public Dimension preferredSize(){
        return new Dimension(50, 50);
    }
}
