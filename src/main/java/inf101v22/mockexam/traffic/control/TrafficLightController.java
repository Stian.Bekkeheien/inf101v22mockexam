package inf101v22.mockexam.traffic.control;

import inf101v22.mockexam.observable.ControlledObservable;
import inf101v22.mockexam.observable.Observable;
import inf101v22.mockexam.traffic.model.TrafficLightControllable;

import java.awt.event.ActionEvent;

import javax.swing.Timer;

public class TrafficLightController implements ITrafficGuiController{
    ControlledObservable<Boolean> isPaused = new ControlledObservable<Boolean>(false);
    TrafficLightControllable controllable;
    Timer timer;
    public TrafficLightController(TrafficLightControllable controllable){
        this.controllable = controllable;
        timer = new Timer(controllable.minMillisInCurrentState(), this::timerFired);
        timer.start();
    }

    private void timerFired(ActionEvent e){
        controllable.goToNextState();
        timer.setInitialDelay(controllable.minMillisInCurrentState());
        timer.restart();

    }

    @Override
    public Observable<Boolean> isPaused() {
        return isPaused;
    }

    @Override
    public void pausePressed(ActionEvent e) {
        isPaused.setValue(true);
        timer.stop();
    }

    @Override
    public void startPressed(ActionEvent e) {
        isPaused.setValue(false);
        timer.start();
    }
}
