package inf101v22.mockexam.traffic.model;


import inf101v22.mockexam.observable.ControlledObservable;
import inf101v22.mockexam.observable.Observable;

public class TrafficLightModel implements TrafficLightControllable{
    ControlledObservable<Boolean> red = new ControlledObservable<Boolean>(null);
    ControlledObservable<Boolean> yellow = new ControlledObservable<Boolean>(null);
    ControlledObservable<Boolean> green = new ControlledObservable<Boolean>(null);


    public TrafficLightModel(){
        red.setValue(false);
        yellow.setValue(false);
        green.setValue(false);
        this.goToNextState();
    }

    @Override
    public Observable<Boolean> redIsOn() {
        return red;
    }

    @Override
    public Observable<Boolean> yellowIsOn() {
        return yellow;
    }

    @Override
    public Observable<Boolean> greenIsOn() {
       return green;
    }

    @Override
    public void goToNextState() {
        if ((green.getValue() == false && yellow.getValue() == false && red.getValue() == false) || (red.getValue() == false && yellow.getValue() == true )){
            red.setValue(true);
            yellow.setValue(false);
            System.out.println("red");
        }
        else if (red.getValue() == true && yellow.getValue() == false){
            yellow.setValue(true);
            
        }

        else if (red.getValue() == true && yellow.getValue() == true){
            red.setValue(false);
            yellow.setValue(false);
            green.setValue(true);
            System.out.println("green");
        }

        else if (green.getValue() == true){
            green.setValue(false);
            yellow.setValue(true);

            System.out.println("yellow");
        }
    }

    @Override
    public int minMillisInCurrentState() {
        int milliseconds = 0;
        if ((green.getValue() == false && yellow.getValue() == false && red.getValue() == true)){
            milliseconds = 2000;
        }
        else if ((green.getValue() == false && yellow.getValue() == true && red.getValue() == true)){
            milliseconds = 500;
            
        }

        else if ((green.getValue() == true && yellow.getValue() == false && red.getValue() == false)){
            milliseconds = 2000;
        }

        else if ((green.getValue() == false && yellow.getValue() == true && red.getValue() == false)){
            milliseconds = 1000;
        }

        return milliseconds;
    }
    
}
